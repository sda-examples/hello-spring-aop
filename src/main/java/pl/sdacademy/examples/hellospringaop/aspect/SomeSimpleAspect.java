package pl.sdacademy.examples.hellospringaop.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SomeSimpleAspect {

    @Pointcut("target(pl.sdacademy.examples.hellospringaop.service.GreetingService)")
    private void greetingService() {
    }

    @Before("greetingService()")
    private void handleBefore() {
        System.out.println("GreetingService is about to be invoked");
    }

    @After("greetingService()")
    private void handleAfter() {
        System.out.println("GreetingService has been invoked");
    }
}

package pl.sdacademy.examples.hellospringaop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import pl.sdacademy.examples.hellospringaop.service.GreetingService;

@EnableAspectJAutoProxy
@ComponentScan("pl.sdacademy.examples.hellospringaop")
public class Application {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
        GreetingService service = context.getBean(GreetingService.class);
        service.sayHello();
    }
}

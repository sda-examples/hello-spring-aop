package pl.sdacademy.examples.hellospringaop.service;

import org.springframework.stereotype.Component;

@Component
public class GreetingService {

    public void sayHello() {
        System.out.println("Hello, World!");
    }
}
